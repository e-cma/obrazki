﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace OBRAZKI 
{

    class Obraz : IComparable<Obraz>
    {
        public double Wartosc;
        public int Klasyfikacja;

        public Obraz(Double war, int klas)
        {
            Wartosc = war;
            Klasyfikacja = klas;
        }

        public int CompareTo(Obraz o)
        {
            if (Wartosc > o.Wartosc) return 1;
            else return -1;

        }
    }
}
