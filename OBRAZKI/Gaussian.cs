﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace OBRAZKI
{
   
    class Gaussian
    {
        Random rand;

        /// <summary>
        /// Konstruktor
        /// </summary>
        public Gaussian()
        {
            rand = new Random();

        }

        /// <summary>
        /// Funkcja losujaca
        /// </summary>
        /// <param name="srednia">srednia wartosc losowanyc liczb</param>
        /// <param name="mnoznik">jakis mnoznik</param>
        /// <returns></returns>
        public double Random(int srednia, int mnoznik)
        {
            return srednia + mnoznik * (Math.Sqrt(-2.0 * Math.Log(rand.NextDouble())) * Math.Sin(2.0 * Math.PI * rand.NextDouble()));
        }

    }
}
