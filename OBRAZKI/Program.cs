﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.IO;

namespace OBRAZKI
{
    class Program
    {
        static int ROZMIAR_CIAGU_UCZACEGO = 1000;
        static double STOSUNEK_CIAGU_1_DO_2 = 0.3;
        static int ILOSC_PRZYKLADPW_TESTOWYCH = 100;
        static int GAUS_1_ULOZENIE = 200;
        static int GAUS_1_SKALA = 100;
        static int GAUS_2_ULOZENIE = 500;
        static int GAUS_2_SKALA = 100;
        static int ROWNOMIERNY_1_MIN = 200;
        static int ROWNOMIERNY_1_MAX = 500;
        static int ROWNOMIERNY_2_MIN = 400;
        static int ROWNOMIERNY_2_MAX = 700;



        static double NormalnyDystrybuantaCalka(double a, double b, double x)
        {
            if (x < a) return 0;
            else if (x <= b) return (x / (b-a))-(a/(b-a));
            else return (b / (b - a)) - (a / (b - a));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="a">dolna wartosc ciagu</param>
        /// <param name="b">gorna wartosc ciagu</param>
        /// <param name="min">dolna wartosc calki</param>
        /// <param name="max">gorna wartosc calki</param>
        /// <returns></returns>
        static double NormalnyDystrybuantaCalka(double a, double b, double min, double max)
        {
            double val;

            val = NormalnyDystrybuantaCalka(a, b, max) - NormalnyDystrybuantaCalka(a, b, min);

            return val;
        }


        static double Erf(double x)
        {
            // constants
            double a1 = 0.254829592;
            double a2 = -0.284496736;
            double a3 = 1.421413741;
            double a4 = -1.453152027;
            double a5 = 1.061405429;
            double p = 0.3275911;

            // Save the sign of x
            int sign = 1;
            if (x < 0)
                sign = -1;
            x = Math.Abs(x);

            // A&S formula 7.1.26
            double t = 1.0 / (1.0 + p * x);
            double y = 1.0 - (((((a5 * t + a4) * t) + a3) * t + a2) * t + a1) * t * Math.Exp(-x * x);

            return sign * y;
        }


        /// <summary>
        /// Gestosc przwdopodobienstaw dla rozkaldu gaussa
        /// </summary>
        /// <param name="uloz">Ułożenie (mediana)</param>
        /// <param name="skal">Skala</param>
        /// <param name="x">Gestość w punkcie x</param>
        /// <returns></returns>
        static double GestoscRozkladGausa(int uloz, int skal, double x)
        {
            
            return ((1 / (skal * Math.Sqrt(2 * Math.PI))) * (Math.Exp(-(Math.Pow((x - uloz), 2)) / (2 * Math.Pow(skal, 2)))));
        }


        /// <summary>
        /// Wejscie
        /// Program ->liczenie wedlug kompilacji
        /// Program Gaus ROZMIARCIAGUUCZACEGO STOSUNEKCIAGU1DO2 ILOSCPRZYKLADOWTESTOWYCH G1ULOZENIE G1SKALA G2ULOZENIE G2SKALA->liczenie ciagu gausa dla parametrow
        /// Program Rownomierny ROZMIARCIAGUUCZACEGO STOSUNEKCIAGU1DO2 ILOSCPRZYKLADOWTESTOWYCH R1MIN R1MAX R2MIN R2MAX ->liczenie ciagu rownomiernego dla parametrow
        /// </summary>
        /// <param name="args"></param>
        static void Main(string[] args)
        {
            bool ZAPISZGAUSA = false;
            bool ZAPISZROWNOMIERNY = false;
            if(args.Length>0)
            {
                if(args[0].Contains("Gaus"))
                {
                    ZAPISZGAUSA = true;
                    ROZMIAR_CIAGU_UCZACEGO = int.Parse(args[1]);
                    
                    STOSUNEK_CIAGU_1_DO_2 = 0.1*double.Parse(args[2], System.Globalization.NumberStyles.AllowDecimalPoint, System.Globalization.NumberFormatInfo.InvariantInfo);
                    ILOSC_PRZYKLADPW_TESTOWYCH = int.Parse(args[3]);
                    GAUS_1_ULOZENIE = int.Parse(args[4]);
                    GAUS_1_SKALA = int.Parse(args[5]);
                    GAUS_2_ULOZENIE = int.Parse(args[6]);
                    GAUS_2_SKALA = int.Parse(args[7]);
                }
                else
                {

                        ZAPISZROWNOMIERNY = true;
                        ROZMIAR_CIAGU_UCZACEGO = int.Parse(args[1]);
                        STOSUNEK_CIAGU_1_DO_2 =0.1* double.Parse(args[2]);
                        ILOSC_PRZYKLADPW_TESTOWYCH = int.Parse(args[3]);
                        ROWNOMIERNY_1_MIN = int.Parse(args[4]);
                        ROWNOMIERNY_1_MAX = int.Parse(args[4])+int.Parse(args[5]);
                        ROWNOMIERNY_2_MIN = int.Parse(args[6]);
                        ROWNOMIERNY_2_MAX = int.Parse(args[6])+int.Parse(args[7]);
                    
                }
            }



            ///Genero
            List<Obraz> CiagGausa = new List<Obraz>();
            List<double> CiagGausa1 = new List<double>();
            float SredniaGausa1 = 0;
            List<double> CiagGausa2 = new List<double>();
            float SredniaGausa2 = 0;

            List<Obraz> CiagRownomierny = new List<Obraz>();
            List<double> CiagRownomierny1 = new List<double>();
            float SredniaRownomierna1 = 0;
            List<double> CiagRownomierny2 = new List<double>();
            float SredniaRownomierna2 = 0;
            Random rand = new Random();

            #region Generowanie_Ciagow_Uczacych

            Gaussian generator = new Gaussian();
            for (int i = 0; i < ROZMIAR_CIAGU_UCZACEGO; ++i)
            {
                CiagGausa1.Add(generator.Random(GAUS_1_ULOZENIE, GAUS_1_SKALA));
                SredniaGausa1 += (float)CiagGausa1[i];
                CiagGausa.Add(new Obraz(CiagGausa1[i], 1));
                CiagGausa2.Add(generator.Random(GAUS_2_ULOZENIE, GAUS_2_SKALA));
                SredniaGausa2 += (float)CiagGausa2[i];
                CiagGausa.Add(new Obraz(CiagGausa2[i], 2));

                CiagRownomierny1.Add(ROWNOMIERNY_1_MIN + ((ROWNOMIERNY_1_MAX-ROWNOMIERNY_1_MIN) * rand.NextDouble()));
                SredniaRownomierna1 += (float)CiagRownomierny1[i];
                CiagRownomierny.Add(new Obraz(CiagRownomierny1[i], 1));
                CiagRownomierny2.Add(ROWNOMIERNY_2_MIN + ((ROWNOMIERNY_2_MAX-ROWNOMIERNY_2_MIN) * rand.NextDouble()));
                SredniaRownomierna2 += (float)CiagRownomierny2[i];
                CiagRownomierny.Add(new Obraz(CiagRownomierny2[i], 2));
            }


            CiagGausa.Sort();
            CiagRownomierny.Sort();

            CiagGausa1.Sort();
            CiagGausa2.Sort();
            CiagRownomierny1.Sort();
            CiagRownomierny2.Sort();

            SredniaGausa1 = SredniaGausa1 / ROZMIAR_CIAGU_UCZACEGO;
            SredniaGausa2 = SredniaGausa2 / ROZMIAR_CIAGU_UCZACEGO;
            SredniaRownomierna1 = SredniaRownomierna1 / ROZMIAR_CIAGU_UCZACEGO;
            SredniaRownomierna2 = SredniaRownomierna2 / ROZMIAR_CIAGU_UCZACEGO;

            #endregion
            /*
            Console.WriteLine(SredniaGausa1);
            Console.WriteLine(SredniaGausa2);
            Console.WriteLine(SredniaRownomierna1);
            Console.WriteLine(SredniaRownomierna2);
            */
            int GausZlePrzypozadkowanie_NM = 0;
            int GausZlePrzypozadkowanie_1NN = 0;
            int GausZlePrzypozadkowanie_3NN = 0;
            int GausZlePrzypozadkowanie_5NN = 0;
            int GausZlePrzypozadkowanie_135NN = 0;
            int GausZlePrzypozadkowanie_Bayess = 0;
            int GausZlePrzypozadkowanie_Bayess_2 = 0;
            int GausZlePrzypozadkowanie_Bayess_1 = 0;

            int RowZlePrzypozadkowanie_NM = 0;
            int RowZlePrzypozadkowanie_1NN = 0;
            int RowZlePrzypozadkowanie_3NN = 0;
            int RowZlePrzypozadkowanie_5NN = 0;
            int RowZlePrzypozadkowanie_135NN = 0;
            int RowZlePrzypozadkowanie_Bayess = 0;
            int RowZlePrzypozadkowanie_Bayess_2 = 0;
            int RowZlePrzypozadkowanie_Bayess_1 = 0;

            List<double> TestowyGausa1 = new List<double>();
            List<double> TestowyGausa2 = new List<double>();
            List<double> TestowyRownomierny1 = new List<double>();
            List<double> TestowyRownomierny2 = new List<double>();

            #region Generowanie_Ciagow_Testowych

            for (int i = 0; i < ILOSC_PRZYKLADPW_TESTOWYCH - (ILOSC_PRZYKLADPW_TESTOWYCH * STOSUNEK_CIAGU_1_DO_2); ++i)
            {
                TestowyGausa1.Add(generator.Random(GAUS_1_ULOZENIE, GAUS_1_SKALA));

                TestowyRownomierny1.Add(ROWNOMIERNY_1_MIN + ((ROWNOMIERNY_1_MAX - ROWNOMIERNY_1_MIN) * rand.NextDouble()));

            }

            for (int i = 0; i < ILOSC_PRZYKLADPW_TESTOWYCH * STOSUNEK_CIAGU_1_DO_2; ++i)
            {

                TestowyGausa2.Add(generator.Random(GAUS_2_ULOZENIE, GAUS_2_SKALA));

                TestowyRownomierny2.Add(ROWNOMIERNY_2_MIN + ((ROWNOMIERNY_2_MAX - ROWNOMIERNY_2_MIN) * rand.NextDouble()));
            }

            #endregion

            #region Testy_NM


            foreach (double x in TestowyGausa1)
            {
                if (Math.Abs(x - SredniaGausa1) > Math.Abs(x - SredniaGausa2))
                {
                    GausZlePrzypozadkowanie_NM++;
                }
            }

            foreach (double x in TestowyGausa2)
            {
                if (Math.Abs(x - SredniaGausa1) < Math.Abs(x - SredniaGausa2))
                {
                    GausZlePrzypozadkowanie_NM++;
                }
            }

            foreach (double x in TestowyRownomierny1)
            {
                if (Math.Abs(x - SredniaRownomierna1) > Math.Abs(x - SredniaRownomierna2))
                {
                    RowZlePrzypozadkowanie_NM++;
                }
            }

            foreach (double x in TestowyRownomierny2)
            {

                if (Math.Abs(x - SredniaRownomierna1) < Math.Abs(x - SredniaRownomierna2))
                {
                    RowZlePrzypozadkowanie_NM++;
                }
            }
            #endregion

            #region Testy_135NN


            ///Testowy gausa 2
            for (int i = 0; i < TestowyGausa2.Count; ++i)
            {
                int k = 0;
                for (k = 0; k < CiagGausa.Count - 1; ++k)
                {
                    if (CiagGausa[k].Wartosc > TestowyGausa2[i])
                    {
                        break;
                    }
                }

                Obraz[] NajblizsziSasiedzi = { new Obraz(400000, 1), new Obraz(400000, 1), new Obraz(400000, 1), new Obraz(400000, 1), new Obraz(400000, 1) };
                for (int l = -5; l <= 5; ++l)
                {

                    try
                    {
                        for (int m = 0; m < 5; ++m)
                        {
                            if (Math.Abs(CiagGausa[k + l].Wartosc - TestowyGausa2[i]) < Math.Abs(NajblizsziSasiedzi[m].Wartosc - TestowyGausa2[i]))
                            {
                                NajblizsziSasiedzi[m] = CiagGausa[k + l];
                                break;
                            }
                        }
                    }
                    catch (Exception ex)
                    { }
                }
                int l1 = 0;
                int l2 = 0;
                int zakl1 = 0, zakl3 = 0, zakl5 = 0;
                for (int m = 0; m < 5; ++m)
                {
                    if (NajblizsziSasiedzi[m].Klasyfikacja == 1) l1++;
                    else l2++;
                    if (m == 0)
                    {
                        if (l1 > l2)
                        {
                            zakl1 = 1;
                        }
                        else
                        {
                            zakl1 = 2;
                        }
                    }
                    if (m == 2)
                    {
                        if (l1 > l2)
                        {
                            zakl3 = 1;
                        }
                        else
                        {
                            zakl3 = 2;
                        }
                    }

                }

                if (l1 > l2)
                {
                    zakl5 = 1;
                }
                else
                {
                    zakl5 = 2;
                }

                if (zakl1 == 1) GausZlePrzypozadkowanie_1NN++;
                if (zakl3 == 1) GausZlePrzypozadkowanie_3NN++;
                if (zakl5 == 1) GausZlePrzypozadkowanie_5NN++;

                if ((zakl1 + zakl3 + zakl5) < 5) GausZlePrzypozadkowanie_135NN++;


            }

            ///Testowy Gausa 1
            for (int i = 0; i < TestowyGausa1.Count; ++i)
            {
                int k = 0;
                for (k = 0; k < CiagGausa.Count - 1; ++k)
                {
                    if (CiagGausa[k].Wartosc > TestowyGausa1[i])
                    {
                        break;
                    }
                }

                Obraz[] NajblizsziSasiedzi = { new Obraz(400000, 1), new Obraz(400000, 1), new Obraz(400000, 1), new Obraz(400000, 1), new Obraz(400000, 1) };
                for (int l = -5; l <= 5; ++l)
                {

                    try
                    {
                        for (int m = 0; m < 5; ++m)
                        {
                            if (Math.Abs(CiagGausa[k + l].Wartosc - TestowyGausa1[i]) < Math.Abs(NajblizsziSasiedzi[m].Wartosc - TestowyGausa1[i]))
                            {
                                NajblizsziSasiedzi[m] = CiagGausa[k + l];
                                break;
                            }
                        }
                    }
                    catch (Exception ex)
                    { }
                }
                int l1 = 0;
                int l2 = 0;
                int zakl1 = 0, zakl3 = 0, zakl5 = 0;
                for (int m = 0; m < 5; ++m)
                {
                    if (NajblizsziSasiedzi[m].Klasyfikacja == 1) l1++;
                    else l2++;
                    if (m == 0)
                    {
                        if (l1 > l2)
                        {
                            zakl1 = 1;
                        }
                        else
                        {
                            zakl1 = 2;
                        }
                    }
                    if (m == 2)
                    {
                        if (l1 > l2)
                        {
                            zakl3 = 1;
                        }
                        else
                        {
                            zakl3 = 2;
                        }
                    }

                }

                if (l1 > l2)
                {
                    zakl5 = 1;
                }
                else
                {
                    zakl5 = 2;
                }

                if (zakl1 == 2) GausZlePrzypozadkowanie_1NN++;
                if (zakl3 == 2) GausZlePrzypozadkowanie_3NN++;
                if (zakl5 == 2) GausZlePrzypozadkowanie_5NN++;

                if ((zakl1 + zakl3 + zakl5) > 4) GausZlePrzypozadkowanie_135NN++;


            }

            //Testowy rownomierny1
            for (int i = 0; i < TestowyRownomierny1.Count; ++i)
            {
                int k = 0;
                for (k = 0; k < CiagRownomierny.Count - 1; ++k)
                {
                    if (CiagRownomierny[k].Wartosc > TestowyRownomierny1[i])
                    {
                        break;
                    }
                }

                Obraz[] NajblizsziSasiedzi = { new Obraz(400000, 1), new Obraz(400000, 1), new Obraz(400000, 1), new Obraz(400000, 1), new Obraz(400000, 1) };
                for (int l = -5; l <= 5; ++l)
                {

                    try
                    {
                        for (int m = 0; m < 5; ++m)
                        {
                            if (Math.Abs(CiagRownomierny[k + l].Wartosc - TestowyRownomierny1[i]) < Math.Abs(NajblizsziSasiedzi[m].Wartosc - TestowyRownomierny1[i]))
                            {
                                NajblizsziSasiedzi[m] = CiagRownomierny[k + l];
                                break;
                            }
                        }
                    }
                    catch (Exception ex)
                    { }
                }
                int l1 = 0;
                int l2 = 0;
                int zakl1 = 0, zakl3 = 0, zakl5 = 0;
                for (int m = 0; m < 5; ++m)
                {
                    if (NajblizsziSasiedzi[m].Klasyfikacja == 1) l1++;
                    else l2++;
                    if (m == 0)
                    {
                        if (l1 > l2)
                        {
                            zakl1 = 1;
                        }
                        else
                        {
                            zakl1 = 2;
                        }
                    }
                    if (m == 2)
                    {
                        if (l1 > l2)
                        {
                            zakl3 = 1;
                        }
                        else
                        {
                            zakl3 = 2;
                        }
                    }

                }

                if (l1 > l2)
                {
                    zakl5 = 1;
                }
                else
                {
                    zakl5 = 2;
                }

                if (zakl1 == 1) RowZlePrzypozadkowanie_1NN++;
                if (zakl3 == 1) RowZlePrzypozadkowanie_3NN++;
                if (zakl5 == 1) RowZlePrzypozadkowanie_5NN++;

                if ((zakl1 + zakl3 + zakl5) < 5) RowZlePrzypozadkowanie_135NN++;


            }

            //Testowy rownomierny2
            for (int i = 0; i < TestowyRownomierny2.Count; ++i)
            {
                int k = 0;
                for (k = 0; k < CiagRownomierny.Count - 1; ++k)
                {
                    if (CiagRownomierny[k].Wartosc > TestowyRownomierny2[i])
                    {
                        break;
                    }
                }

                Obraz[] NajblizsziSasiedzi = { new Obraz(400000, 1), new Obraz(400000, 1), new Obraz(400000, 1), new Obraz(400000, 1), new Obraz(400000, 1) };
                for (int l = -5; l <= 5; ++l)
                {

                    try
                    {
                        for (int m = 0; m < 5; ++m)
                        {
                            if (Math.Abs(CiagRownomierny[k + l].Wartosc - TestowyRownomierny2[i]) < Math.Abs(NajblizsziSasiedzi[m].Wartosc - TestowyRownomierny2[i]))
                            {
                                NajblizsziSasiedzi[m] = CiagRownomierny[k + l];
                                break;
                            }
                        }
                    }
                    catch (Exception ex)
                    { }
                }
                int l1 = 0;
                int l2 = 0;
                int zakl1 = 0, zakl3 = 0, zakl5 = 0;
                for (int m = 0; m < 5; ++m)
                {
                    if (NajblizsziSasiedzi[m].Klasyfikacja == 1) l1++;
                    else l2++;
                    if (m == 0)
                    {
                        if (l1 > l2)
                        {
                            zakl1 = 1;
                        }
                        else
                        {
                            zakl1 = 2;
                        }
                    }
                    if (m == 2)
                    {
                        if (l1 > l2)
                        {
                            zakl3 = 1;
                        }
                        else
                        {
                            zakl3 = 2;
                        }
                    }

                }

                if (l1 > l2)
                {
                    zakl5 = 1;
                }
                else
                {
                    zakl5 = 2;
                }

                if (zakl1 == 1) RowZlePrzypozadkowanie_1NN++;
                if (zakl3 == 1) RowZlePrzypozadkowanie_3NN++;
                if (zakl5 == 1) RowZlePrzypozadkowanie_5NN++;

                if ((zakl1 + zakl3 + zakl5) < 5) RowZlePrzypozadkowanie_135NN++;


            }


            #endregion

            #region Klasyfikacja_Bayesowska


            foreach (double x in TestowyGausa1)
            {
                if (((1 - STOSUNEK_CIAGU_1_DO_2) * GestoscRozkladGausa(GAUS_1_ULOZENIE, GAUS_1_SKALA, x)) < ((STOSUNEK_CIAGU_1_DO_2) * GestoscRozkladGausa(GAUS_2_ULOZENIE, GAUS_2_SKALA, x)))
                {
                    GausZlePrzypozadkowanie_Bayess++;
                    GausZlePrzypozadkowanie_Bayess_1++;
                }
            }

            foreach (double x in TestowyGausa2)
            {
                if (((1 - STOSUNEK_CIAGU_1_DO_2) * GestoscRozkladGausa(GAUS_1_ULOZENIE, GAUS_1_SKALA, x)) > ((STOSUNEK_CIAGU_1_DO_2) * GestoscRozkladGausa(GAUS_2_ULOZENIE, GAUS_2_SKALA, x)))
                {
                    GausZlePrzypozadkowanie_Bayess++;
                    GausZlePrzypozadkowanie_Bayess_2++;
                }
            }

            foreach (double x in TestowyRownomierny1)
            {
                double gestoscRow1=0;
                double gestoscRow2=0;
                if(x<CiagRownomierny1[CiagRownomierny1.Count-1])gestoscRow1=1/(CiagRownomierny1[CiagRownomierny1.Count-1]-CiagRownomierny1[0]);
                if(x>CiagRownomierny2[0])gestoscRow2=1/(CiagRownomierny2[CiagRownomierny2.Count-1]-CiagRownomierny2[0]);
                
                if (((1 - STOSUNEK_CIAGU_1_DO_2) * gestoscRow1) < ((STOSUNEK_CIAGU_1_DO_2) * gestoscRow2))
                {
                    RowZlePrzypozadkowanie_Bayess++;
                    RowZlePrzypozadkowanie_Bayess_1++;
                }
            }

            foreach (double x in TestowyGausa2)
            {
                double gestoscRow1 = 0;
                double gestoscRow2 = 0;
                if (x < CiagRownomierny1[CiagRownomierny1.Count - 1]) gestoscRow1 = 1 / (CiagRownomierny1[CiagRownomierny1.Count - 1] - CiagRownomierny1[0]);
                if (x > CiagRownomierny2[0]) gestoscRow2 = 1 / (CiagRownomierny2[CiagRownomierny2.Count - 1] - CiagRownomierny2[0]);

                if (((1 - STOSUNEK_CIAGU_1_DO_2) * gestoscRow1) > ((STOSUNEK_CIAGU_1_DO_2) * gestoscRow2))
                {
                    RowZlePrzypozadkowanie_Bayess++;
                    RowZlePrzypozadkowanie_Bayess_2++;
                }
            }


            #endregion

            #region Ryzyko_Dla_01_Funkcji_Strat

            ///Tu bylo coś źle
            ///trzeba wyznaczyć punkt w którym prawdopodobieństwa obu rozkałdaów są jednakowe i liczyć od niego całkę nie jestem pewien czy wystarczy mediana... 
            ///
            ///po wywalanieu z rónania złych przyporządkowań daje prawdopodobne wyniki

            double Gauss1RyzykoFunkcjiStrat = 0;
            double Gauss2RyzykoFunkcjiStrat = 0;
            double Row1RyzykoFunkcjiStrat = 0;
            double Row2RyzykoFunkcjiStrat = 0;

            double WartoscSrodkowa = 0;

            Gauss1RyzykoFunkcjiStrat = (1 - STOSUNEK_CIAGU_1_DO_2) * 1 * ((-0.5) * Erf((GAUS_1_ULOZENIE - CiagGausa[CiagGausa.Count / 2].Wartosc) / (GAUS_1_SKALA * Math.Sqrt(2))) - (-0.5) * Erf((GAUS_1_ULOZENIE - CiagGausa1[0]) / (GAUS_1_SKALA * Math.Sqrt(2))));


            Gauss2RyzykoFunkcjiStrat = (STOSUNEK_CIAGU_1_DO_2) * /*GausZlePrzypozadkowanie_Bayess_2*/1 * ((-0.5) * Erf((GAUS_2_ULOZENIE - CiagGausa2[CiagGausa2.Count - 1]) / (GAUS_2_SKALA * Math.Sqrt(2))) - (-0.5) * Erf((GAUS_2_ULOZENIE - CiagGausa[CiagGausa.Count / 2].Wartosc) / (GAUS_2_SKALA * Math.Sqrt(2))));

            Row1RyzykoFunkcjiStrat = (1-STOSUNEK_CIAGU_1_DO_2) * /*GausZlePrzypozadkowanie_Bayess_2*/1 * NormalnyDystrybuantaCalka(ROWNOMIERNY_1_MIN,ROWNOMIERNY_1_MAX,ROWNOMIERNY_1_MIN,((double)(ROWNOMIERNY_1_MIN+ROWNOMIERNY_2_MAX)/2));

            Row2RyzykoFunkcjiStrat = (STOSUNEK_CIAGU_1_DO_2) * /*GausZlePrzypozadkowanie_Bayess_2*/1 * NormalnyDystrybuantaCalka(ROWNOMIERNY_2_MIN, ROWNOMIERNY_2_MAX, ((double)(ROWNOMIERNY_1_MIN + ROWNOMIERNY_2_MAX) / 2), ROWNOMIERNY_2_MAX);

            #endregion


            /*
            Console.Write("GausZlePrzypozadkowanie_NM: "); Console.WriteLine(GausZlePrzypozadkowanie_NM);
            Console.Write("GausZlePrzypozadkowanie_135NN: "); Console.WriteLine(GausZlePrzypozadkowanie_135NN);
            Console.Write("GausZlePrzypozadkowanie_Bayess: "); Console.WriteLine(GausZlePrzypozadkowanie_Bayess);
            Console.Write("Gauss_1_RyzykoFunkcjiStrat: "); Console.WriteLine(Gauss1RyzykoFunkcjiStrat);
            Console.Write("Gauss_2_RyzykoFunkcjiStrat: "); Console.WriteLine(Gauss2RyzykoFunkcjiStrat);

            Console.WriteLine("");

            Console.Write("RowZlePrzypozadkowanie_NM: "); Console.WriteLine(RowZlePrzypozadkowanie_NM);
            Console.Write("RowZlePrzypozadkowanie_135NN: "); Console.WriteLine(RowZlePrzypozadkowanie_135NN);
            Console.Write("RowZlePrzypozadkowanie_Bayess: "); Console.WriteLine(RowZlePrzypozadkowanie_Bayess);
            Console.Write("Row_1_RyzykoFunkcjiStrat: "); Console.WriteLine(Row1RyzykoFunkcjiStrat);
            Console.Write("Row_2_RyzykoFunkcjiStrat: "); Console.WriteLine(Row2RyzykoFunkcjiStrat);
            //Console.WriteLine(RowZlePrzypozadkowanie_NM);
           // Console.ReadLine();
            */

            if(ZAPISZGAUSA)
            {
                
                //StreamWriter file = new StreamWriter("GAUSS");
                if(!File.Exists("GAUSS.csv"))
                {
                    File.WriteAllText("GAUSS.csv", "typ;rozmiar_ciagu_uczacego;stosunek_ciagu_1_do_2;ilosc_przykladow_testowych;G1_ulozenie;G1_skala;G2_ulozenie;G2_skala;GausZlePrzypozadkowanie_NM;GausZlePrzypozadkowanie_135NN;GausZlePrzypozadkowanie_Bayess;Gauss_RyzykoFunkcjiStrat\n");
                  
                }

                StreamWriter plik = new StreamWriter("GAUSS.csv", true);
                    plik.WriteLine("Gaus;" + ROZMIAR_CIAGU_UCZACEGO.ToString() + ";" + STOSUNEK_CIAGU_1_DO_2.ToString() + ";" + ILOSC_PRZYKLADPW_TESTOWYCH.ToString() + ";" + GAUS_1_ULOZENIE.ToString() + ";" + GAUS_1_SKALA.ToString() + ";" + GAUS_2_ULOZENIE.ToString() + ";" + GAUS_2_SKALA.ToString() + ";" + GausZlePrzypozadkowanie_NM.ToString() + ";" + GausZlePrzypozadkowanie_135NN.ToString() + ";" + GausZlePrzypozadkowanie_Bayess.ToString() + ";" + (Gauss1RyzykoFunkcjiStrat +  Gauss2RyzykoFunkcjiStrat).ToString());
                    plik.Close();
            }

            if (ZAPISZROWNOMIERNY)
            {

                //StreamWriter file = new StreamWriter("GAUSS");
                if (!File.Exists("ROWNOMIERNY.csv"))
                {
                    File.WriteAllText("ROWNOMIERNY.csv","typ;rozmiar_ciagu_uczacego;stosunek_ciagu_1_do_2;ilosc_przykladow_testowych;R1_MIN;R1_MAX;R2_MIN;R2_MAX;RowZlePrzypozadkowanie_NM;RowZlePrzypozadkowanie_135NN;RowZlePrzypozadkowanie_Bayess;Row_RyzykoFunkcjiStrat\n");


                }

                StreamWriter plik = new StreamWriter("ROWNOMIERNY.csv", true);

                plik.WriteLine("Rownomierny;" + ROZMIAR_CIAGU_UCZACEGO.ToString() + ";" + STOSUNEK_CIAGU_1_DO_2.ToString() + ";" + ILOSC_PRZYKLADPW_TESTOWYCH.ToString() + ";" + ROWNOMIERNY_1_MIN + ";" + ROWNOMIERNY_1_MAX + ";" + ROWNOMIERNY_2_MIN + ";" + ROWNOMIERNY_2_MAX + ";" + RowZlePrzypozadkowanie_NM.ToString() + ";" + RowZlePrzypozadkowanie_135NN.ToString() + ";" + RowZlePrzypozadkowanie_Bayess.ToString() + ";" + (Row1RyzykoFunkcjiStrat + Row2RyzykoFunkcjiStrat).ToString());
                plik.Close();
            }
        }
    }
}
